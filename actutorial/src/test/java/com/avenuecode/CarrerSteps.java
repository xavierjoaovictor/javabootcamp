package com.avenuecode;

import static org.junit.Assert.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import com.avenuecode.pages.*;
import com.thoughtworks.selenium.webdriven.commands.Open;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class CarrerSteps {
	WebDriver driver;
	
	@Before
	public void start(){
		driver = new ChromeDriver();
	}
	
	@After
	public void tearDown(){
		driver.quit();
	}
	
	@Given("^I visit Avenue Code website$")
	public void i_visit_Avenue_Code_website() throws Throwable {
	    driver.get("https://www.avenuecode.com");
	}

	@When("^I navigate to Careers page$")
	public void i_navigate_to_Careers_page() throws Throwable {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.navigateToCareerPage();
		CareerPage careerPage = PageFactory.initElements(driver, CareerPage.class);
		careerPage.navigateToAllOpeningsPage();
	}

	@Then("^I should see at least \\$(\\d+) job locations$")
	public void i_should_see_at_least_$_job_locations(int count) throws Throwable {
		OpenPositionsPage openPage = PageFactory.initElements(driver, OpenPositionsPage.class);
		assertThat(openPage.cityTotalCount(), greaterThanOrEqualTo(count));
	}

}
