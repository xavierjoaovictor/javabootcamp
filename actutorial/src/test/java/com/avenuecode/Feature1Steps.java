package com.avenuecode;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import com.avenuecode.pages.*;
import com.thoughtworks.selenium.webdriven.commands.Open;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class Feature1Steps {
	WebDriver driver;
	
	@Before
	public void start(){
		driver = new ChromeDriver();
	}
	
	@After
	public void tearDown(){
		driver.quit();
	}
	
	@Then("^I should see \\$(\\d+) latest news$")
	public void i_should_see_$_latest_news(int arg1) throws Throwable {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		assertThat(homePage.newsCount(), greaterThanOrEqualTo(arg1));
	}
	
	@When("^I click in a new's link$")
	public void i_click_in_a_new_s_link() throws Throwable {
	    HomePage homePage = PageFactory.initElements(driver, HomePage.class);
	    homePage.navigateToNewsPage();
	}

	@Then("^I should be redirect to the new's content$")
	public void i_should_be_redirect_to_the_new_s_content() throws Throwable {

//		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs2.get(1));
	    driver.close();
	    assertThat(tabs2.size(), greaterThanOrEqualTo(2) );
	}

}
