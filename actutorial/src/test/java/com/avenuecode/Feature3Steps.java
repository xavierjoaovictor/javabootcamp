package com.avenuecode;

import static org.junit.Assert.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import com.avenuecode.pages.*;
import com.thoughtworks.selenium.webdriven.commands.Open;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class Feature3Steps {
	
	
	@When("^I navigate to Events page$")
	public void i_navigate_to_Events_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@When("^I see a list of locations$")
	public void i_see_a_list_of_locations() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^I should chose San Francisco to filter$")
	public void i_should_chose_San_Francisco_to_filter() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^I should chose Belo Horizonte to filter$")
	public void i_should_chose_Belo_Horizonte_to_filter() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^I should chose São Paulo to filter$")
	public void i_should_chose_São_Paulo_to_filter() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}
}
