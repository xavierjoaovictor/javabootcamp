package com.avenuecode.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

public class HomePage {

	@FindBy(linkText = "Careers")
	private WebElement careerLink;
	
	@FindBys({
		@FindBy(xpath = "//*[@id='sub-footer-news']/div/div")
	})
	List<WebElement> newsContentItems;

	@FindBy(xpath = "//*[@id='sub-footer-news']/div/div[1]/a")
	private WebElement newsLink;
	
	public void navigateToCareerPage(){
		careerLink.click();
	}
	
	public int newsCount(){
		return newsContentItems.size();
	}
	
	public void navigateToNewsPage(){
		newsLink.click();
	}
	
	public String newsLinkhref(){
		return newsLink.getAttribute("href").toString();
	}
}
