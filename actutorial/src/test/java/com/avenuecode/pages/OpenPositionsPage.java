package com.avenuecode.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

public class OpenPositionsPage {
	@FindBys({
		@FindBy(xpath = "//div[@id='locationSelect']/div[@class='options']/div")
	})
	List<WebElement> cityItems;
	
	public int cityTotalCount(){
		int totalCount = cityItems.size();
		return totalCount;
	}
}
