Feature: As a user I want the footer to display the top three News, when a link is clicked it should redirect to the content

Scenario: Successfully display news
Given I visit Avenue Code website
Then I should see $3 latest news

Scenario: Redirect to the news page
Given I visit Avenue Code website
When I click in a new's link
Then I should be redirect to the new's content