$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("com/avenuecode/Feature1.feature");
formatter.feature({
  "line": 1,
  "name": "As a user I want the footer to display the top three News, when a link is clicked it should redirect to the content",
  "description": "",
  "id": "as-a-user-i-want-the-footer-to-display-the-top-three-news,-when-a-link-is-clicked-it-should-redirect-to-the-content",
  "keyword": "Feature"
});
formatter.before({
  "duration": 2821393328,
  "status": "passed"
});
formatter.before({
  "duration": 1428573601,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Successfully display news",
  "description": "",
  "id": "as-a-user-i-want-the-footer-to-display-the-top-three-news,-when-a-link-is-clicked-it-should-redirect-to-the-content;successfully-display-news",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "I visit Avenue Code website",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I should see $3 latest news",
  "keyword": "Then "
});
formatter.match({
  "location": "CarrerSteps.i_visit_Avenue_Code_website()"
});
formatter.result({
  "duration": 17135440292,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "3",
      "offset": 14
    }
  ],
  "location": "Feature1Steps.i_should_see_$_latest_news(int)"
});
formatter.result({
  "duration": 123760994,
  "error_message": "java.lang.AssertionError: \nExpected: a value equal to or greater than \u003c3\u003e\n     but: \u003c0\u003e was less than \u003c3\u003e\n\tat org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:20)\n\tat org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:8)\n\tat com.avenuecode.Feature1Steps.i_should_see_$_latest_news(Feature1Steps.java:42)\n\tat ✽.Then I should see $3 latest news(com/avenuecode/Feature1.feature:5)\n",
  "status": "failed"
});
formatter.after({
  "duration": 96102430,
  "status": "passed"
});
formatter.after({
  "duration": 79324211,
  "status": "passed"
});
formatter.before({
  "duration": 1385135113,
  "status": "passed"
});
formatter.before({
  "duration": 1381439227,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Redirect to the news page",
  "description": "",
  "id": "as-a-user-i-want-the-footer-to-display-the-top-three-news,-when-a-link-is-clicked-it-should-redirect-to-the-content;redirect-to-the-news-page",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "I visit Avenue Code website",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "I click in a new\u0027s link",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "I should be redirect to the new\u0027s content",
  "keyword": "Then "
});
formatter.match({
  "location": "CarrerSteps.i_visit_Avenue_Code_website()"
});
formatter.result({
  "duration": 5902462763,
  "status": "passed"
});
formatter.match({
  "location": "Feature1Steps.i_click_in_a_new_s_link()"
});
formatter.result({
  "duration": 71702987,
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"//*[@id\u003d\u0027sub-footer-news\u0027]/div/div[1]/a\"}\n  (Session info: chrome\u003d56.0.2924.87)\n  (Driver info: chromedriver\u003d2.27.440174 (e97a722caafc2d3a8b807ee115bfb307f7d2cfd9),platform\u003dMac OS X 10.11.6 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 67 milliseconds\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00272.53.0\u0027, revision: \u002735ae25b1534ae328c771e0856c93e187490ca824\u0027, time: \u00272016-03-15 10:43:46\u0027\nSystem info: host: \u0027ac-aeo-lab-mac.local\u0027, ip: \u0027192.168.2.10\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.11.6\u0027, java.version: \u00271.8.0_121\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.27.440174 (e97a722caafc2d3a8b807ee115bfb307f7d2cfd9), userDataDir\u003d/var/folders/q9/qfcvp6_942sfbbhl47cmzqjm0000gp/T/.org.chromium.Chromium.15WAdh}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d56.0.2924.87, platform\u003dMAC, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 9ec23dfb5dabc2b9c34c9512ee491110\n*** Element info: {Using\u003dxpath, value\u003d//*[@id\u003d\u0027sub-footer-news\u0027]/div/div[1]/a}\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:206)\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:158)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:678)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:363)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:500)\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:361)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:355)\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\n\tat com.sun.proxy.$Proxy15.click(Unknown Source)\n\tat com.avenuecode.pages.HomePage.navigateToNewsPage(HomePage.java:31)\n\tat com.avenuecode.Feature1Steps.i_click_in_a_new_s_link(Feature1Steps.java:48)\n\tat ✽.When I click in a new\u0027s link(com/avenuecode/Feature1.feature:9)\n",
  "status": "failed"
});
formatter.match({
  "location": "Feature1Steps.i_should_be_redirect_to_the_new_s_content()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 90169946,
  "status": "passed"
});
formatter.after({
  "duration": 80015225,
  "status": "passed"
});
formatter.uri("com/avenuecode/Feature2.feature");
formatter.feature({
  "line": 1,
  "name": "As a user I want the footer to display the latest event, when a event is clicked it should redirect to the Events page displaying the modal with the event details",
  "description": "",
  "id": "as-a-user-i-want-the-footer-to-display-the-latest-event,-when-a-event-is-clicked-it-should-redirect-to-the-events-page-displaying-the-modal-with-the-event-details",
  "keyword": "Feature"
});
formatter.before({
  "duration": 1418145361,
  "status": "passed"
});
formatter.before({
  "duration": 1424659385,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Successfully display events",
  "description": "",
  "id": "as-a-user-i-want-the-footer-to-display-the-latest-event,-when-a-event-is-clicked-it-should-redirect-to-the-events-page-displaying-the-modal-with-the-event-details;successfully-display-events",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 2,
      "name": "@WIP"
    }
  ]
});
formatter.step({
  "line": 4,
  "name": "I visit Avenue Code website",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I should see the latest event",
  "keyword": "Then "
});
formatter.match({
  "location": "CarrerSteps.i_visit_Avenue_Code_website()"
});
