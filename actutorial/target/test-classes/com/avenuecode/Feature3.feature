Feature: As a user I want to filter the next events section by location, the locations should be: San Francisco, Belo Horizonte and São Paulo
@WIP
Scenario Outline: Successfully display filter location
Given I visit Avenue Code website
When I navigate to Events page
And I see a list of locations
Then I should chose <location> to filter

Examples:

| location 		 |
| San Francisco  |
| Belo Horizonte |
| São Paulo		 |